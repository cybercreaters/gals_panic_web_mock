var RADIAN = Math.PI / 180; 
var center_x, center_y;
var width, height;
var context;
var imageBack;
var image;
var render;
var camera;
var theta;

window.onload = function()
{
	var canvas = document.getElementById('myCanvas');
	if (! canvas || ! canvas.getContext ) {return false;}
	else
	{
		context = canvas.getContext('2d');
		
		// Canvasのサイズ・中心を取得
		width = canvas.width;
		height = canvas.height;
		center_x = (width*0.5);
		center_y = (height*0.5);
		
		imageBack = new Image();
		imageBack.src = "./back.jpeg";
		
		// 画像ロード
		image = new Image();
		image.onload = init;
		image.src = "./spacer0.jpeg";
	}
};

var init = function()
{  
	// 諸々の初期化
	render = new RenderEngine(context);
	camera = new Camera(0,100,-600,600);
	theta = 0;
	var mat = new BitmapMaterial(image, false);
	
	// 座標計算
	var t = 360 / 12;
	for(var i=0 ; i<12 ; i++)
	{
		var plane = new Plane(0,0,0,70,60,mat);
		var rad = 30*i;
		plane.x = 200 * Math.sin(rad * RADIAN);
		plane.z = 200 * Math.cos(rad * RADIAN);
		plane.rotationY = (rad + 180);
		render.addChild(plane);
	}
	
	setInterval("onFrame()", 30);
};

// 毎フレーム計算
var onFrame = function()
{

	theta ++;
	
	camera.x = 600 * Math.sin(theta * RADIAN);
	camera.z = -600 * Math.cos(theta * RADIAN);
	
	render.rendering(center_x, center_y, width, height, camera);
};
















/*************************************
こっから3D計算用。

・RenderEngine
・Plane
・BitmapMaterial
・Camera
・Matrix3D
・Vector3D

が定義されてる。
removeChildとかsceneとかまだ作ってないです。
***************************************/

/*
	RenderEngine
*/
var RenderEngine = function(ctx)
{
	this.ctx = ctx;
	this.numChildren = 0;
	this.container = [];
};

RenderEngine.prototype.addChild = function(child)
{
	this.container[this.numChildren] = child;
	this.numChildren ++;
};

RenderEngine.prototype.rendering = function(cx, cy, w, h, camera)
{
//	this.ctx.beginPath();
//	this.ctx.fillStyle = "#000000";  
//	this.ctx.fillRect(0,0,w, h);

	/* 画像を描画 */
	this.ctx.drawImage(imageBack, 0, 0, w, h);

	var i;
	for(i=0 ; i<this.numChildren ; i++){ this.container[i].renderCoord(camera, cx, cy);}
	this.container.sort(this.sortFunc);
	for(i=0 ; i<this.numChildren ; i++){ this.container[i].draw(this.ctx);}
	
	this.ctx.font = "50pt Arial";
//	this.ctx.textBaseline = “top”;
//	this.ctx.shadowColor = “#000”;
//	this.ctx.shadowOffsetX = width;
//	this.ctx.shadowOffsetY = 0;
//	this.ctx.shadowBlur = 10;
//	this.ctx.fillText(text, -width, 0);
	this.ctx.fillStyle = "#ff0000";
	this.ctx.fillText("Gals Panic", 10,50);   
	this.ctx.fillStyle = "#EE0066";
	this.ctx.fillText("Gals Panic", 12,52);

	this.ctx.font = "20pt Arial";
	this.ctx.fillStyle = "#FAFAFA";
	this.ctx.fillText("Story1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~", 10,302);
	this.ctx.fillText("Story2 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~", 10,352);
	this.ctx.fillText("Story3 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~", 10,402);
};

RenderEngine.prototype.sortFunc = function(a, b)
{
	return(b.sort - a.sort);
};

/*
	Plane
*/
var Plane = function(x, y, z, width, height, mat)
{
	this.x = x;
	this.y = y;
	this.z = z;
	this.width = width;
	this.height = height;
	this.rotationX = 0;
	this.rotationY = 0;
	this.rotationZ = 0;
	this.material = mat;

	this.sort = 0;
	var w = this.width*0.5;
	var h = this.height*0.5;
	this.original_points =[];
	this.original_points[0] = new Vector3D(-w, h, 0);
	this.original_points[1] = new Vector3D(w, h, 0);
	this.original_points[2] = new Vector3D(-w, -h, 0);
	this.original_points[3] = new Vector3D(w, -h, 0);
	this.points = [];
	for(var i=0 ; i<4 ; i++){ this.points[i] = new Point(0,0);}
};

Plane.prototype.renderCoord = function(camera, cx, cy)
{	
	var view = camera.getViewingTransformMatrix();
	
	var model = new Matrix3D;
	model=model.productParallelTransformMatrix(this.x, this.y, this.z);
	model=model.productRotationZMatrix(this.rotationZ%360);
	model=model.productRotationYMatrix(this.rotationY%360);
	model=model.productRotationXMatrix(this.rotationX%360);

	var mat = view.productMatrix(model);

	this.sort = 0;
	for(var i=0 ; i<4 ; i++)
	{
		var p = mat.productVector(this.original_points[i]);
		this.sort += p.z;
		var scale = camera.focus / camera.CENTER.distance(p);
		this.points[i].x = p.x * scale + cx;
		this.points[i].y = (-1)*p.y * scale + cy;
	}
};

Plane.prototype.draw = function(ctx)
{
	this.material.draw(this.points, ctx);
};

/*
	BitmapMaterial
*/
var BitmapMaterial = function(image, wire)
{
	this.image = image;
	this.wire = wire;
	this.points_d = [];
	this.points_d[0] = new Point(0, 0);
	this.points_d[1] = new Point(image.width, 0);
	this.points_d[2] = new Point(0,image.height);
	this.points_d[3] = new Point(image.width,image.height);
};

BitmapMaterial.prototype.draw = function(points, ctx)
{
	ctx.save();
	ctx.beginPath();
	ctx.moveTo(points[0].x, points[0].y);
	ctx.lineTo(points[1].x, points[1].y);
	ctx.lineTo(points[3].x, points[3].y);
	ctx.lineTo(points[2].x, points[2].y);
	ctx.closePath();
	ctx.clip();
	this.setMatrix(this.points_d[0], this.points_d[1], this.points_d[2], points[0], points[1], points[2], ctx);
	ctx.drawImage(this.image, 0, 0);
	ctx.restore();


	ctx.save();
	ctx.beginPath();
	ctx.moveTo(points[3].x, points[3].y);
	ctx.lineTo(points[1].x, points[1].y);
	ctx.lineTo(points[2].x, points[2].y);
	ctx.closePath();
	ctx.clip();
	this.setMatrix(this.points_d[3], this.points_d[1], this.points_d[2], points[3], points[1], points[2], ctx);
	ctx.drawImage(this.image, 0, 0);
	ctx.restore();
	
	if(this.wire)
	{
		ctx.beginPath();
		ctx.strokeStyle = "#770000";
		ctx.lineWidth = 1;
		ctx.moveTo(points[0].x, points[0].y);
		ctx.lineTo(points[1].x, points[1].y);
		ctx.lineTo(points[2].x, points[2].y);
		ctx.closePath();
		ctx.moveTo(points[3].x, points[3].y);
		ctx.lineTo(points[1].x, points[1].y);
		ctx.lineTo(points[2].x, points[2].y);
		ctx.closePath();
		ctx.stroke();
	}
};

BitmapMaterial.prototype.setMatrix = function(a0, a1, a2, b0, b1, b2, ctx)
{
	var tmp00 = a1.x - a0.x;
	var tmp01 = a1.y - a0.y;
	var tmp10 = a2.x - a0.x;
	var tmp11 = a2.y - a0.y;
	var delta = tmp00 * tmp11 - tmp01 * tmp10;

	var ma00 = tmp11 / delta;
	var ma01 = -1 * tmp01 / delta;
	var ma10 = -1 * tmp10 / delta;
	var ma11 = tmp00 / delta;

	var mb00 = b1.x - b0.x;
	var mb01 = b1.y - b0.y;
	var mb10 = b2.x - b0.x;
	var mb11 = b2.y - b0.y;

	if(mb00 === 0) {mb00 = 0.0001;}
	if(mb01 === 0) {mb01 = 0.0001;}
	if(mb10 === 0) {mb10 = 0.0001;}
	if(mb11 === 0) {mb11 = 0.0001;}

	context.translate(b0.x, b0.y);
	context.transform(mb00, mb01, mb10, mb11, 0, 0);
	context.transform(ma00, ma01, ma10, ma11, 0, 0);
	context.translate(-a0.x, -a0.y);
};

/*
	Camera
*/
var Camera = function(x,y,z,focus)
{
	this.x = x;
	this.y = y;
	this.z = z;
	this.focus = focus;
	
	this.UP = new Vector3D(0,1,0);
	this.CENTER = new Vector3D(0,0,0);
};

Camera.prototype.getViewingTransformMatrix = function()
{
	var camera = new Vector3D(this.x, this.y , this.z);
	var n = this.CENTER.subtract(camera);
	n.normalize();

	var u=this.UP.crossProduct(n);
	u.normalize();

	var v = n.crossProduct(u);
	v.normalize();
	
	var dx = -1*camera.innerProduct(u);
	var dy = -1*camera.innerProduct(v);
	var dz = -1*camera.innerProduct(n);

	var view=new Matrix3D();
	view.set(u.x, u.y, u.z, dx, v.x, v.y, v.z, dy, n.x, n.y, n.z, dz, 0, 0, 0, 1);

	return view;
};

var Point = function(x, y)
{
	this.x = x;
	this.y = y;
};

/*
	Matrix3D
*/
var Matrix3D = function()
{
	this.v11 = 1.0; this.v12 = 0.0; this.v13 = 0.0; this.v14 = 0.0;
	this.v21 = 0.0; this.v22 = 1.0; this.v23 = 0.0; this.v24 = 0.0;
	this.v31 = 0.0; this.v32 = 0.0; this.v33 = 1.0; this.v34 = 0.0;
	this.v41 = 0.0; this.v42 = 0.0; this.v43 = 0.0; this.v44 = 1.0;
	
	this.RADIAN = Math.PI / 180;
};

Matrix3D.prototype.set = function(  v11, v12, v13, v14, 
									v21, v22, v23, v24, 
									v31, v32, v33, v34, 
									v41, v42, v43, v44)
{
	this.v11=v11; this.v12=v12; this.v13=v13; this.v14=v14;
	this.v21=v21; this.v22=v22; this.v23=v23; this.v24=v24;
	this.v31=v31; this.v32=v32; this.v33=v33; this.v34=v34;
	this.v41=v41; this.v42=v42; this.v43=v43; this.v44=v44;
};

Matrix3D.prototype.productMatrix = function(m)
{
	var newMat = new Matrix3D();
	newMat.v11=this.v11 * m.v11 + this.v12 * m.v21 + this.v13 * m.v31 + this.v14 * m.v41;
	newMat.v12=this.v11 * m.v12 + this.v12 * m.v22 + this.v13 * m.v32 + this.v14 * m.v42;
	newMat.v13=this.v11 * m.v13 + this.v12 * m.v23 + this.v13 * m.v33 + this.v14 * m.v43;
	newMat.v14=this.v11 * m.v14 + this.v12 * m.v24 + this.v13 * m.v34 + this.v14 * m.v44;
	newMat.v21=this.v21 * m.v11 + this.v22 * m.v21 + this.v23 * m.v31 + this.v24 * m.v41;
	newMat.v22=this.v21 * m.v12 + this.v22 * m.v22 + this.v23 * m.v32 + this.v24 * m.v42;
	newMat.v23=this.v21 * m.v13 + this.v22 * m.v23 + this.v23 * m.v33 + this.v24 * m.v43;
	newMat.v24=this.v21 * m.v14 + this.v22 * m.v24 + this.v23 * m.v34 + this.v24 * m.v44;
	newMat.v31=this.v31 * m.v11 + this.v32 * m.v21 + this.v33 * m.v31 + this.v34 * m.v41;
	newMat.v32=this.v31 * m.v12 + this.v32 * m.v22 + this.v33 * m.v32 + this.v34 * m.v42;
	newMat.v33=this.v31 * m.v13 + this.v32 * m.v23 + this.v33 * m.v33 + this.v34 * m.v43;
	newMat.v34=this.v31 * m.v14 + this.v32 * m.v24 + this.v33 * m.v34 + this.v34 * m.v44;
	newMat.v41=this.v41 * m.v11 + this.v42 * m.v21 + this.v43 * m.v31 + this.v44 * m.v41;
	newMat.v42=this.v41 * m.v12 + this.v42 * m.v22 + this.v43 * m.v32 + this.v44 * m.v42;
	newMat.v43=this.v41 * m.v13 + this.v42 * m.v23 + this.v43 * m.v33 + this.v44 * m.v43;
	newMat.v44=this.v41 * m.v14 + this.v42 * m.v24 + this.v43 * m.v34 + this.v44 * m.v44;
	return newMat;
};

Matrix3D.prototype.productVector = function(v)
{
	var newVec = new Vector3D();
	newVec.x=this.v11 * v.x + this.v12 * v.y + this.v13 * v.z + this.v14;
	newVec.y=this.v21 * v.x + this.v22 * v.y + this.v23 * v.z + this.v24;
	newVec.z=this.v31 * v.x + this.v32 * v.y + this.v33 * v.z + this.v34;
	return newVec;
};

Matrix3D.prototype.productRotationXMatrix = function(rotationX)
{
	var sin=Math.sin(rotationX * this.RADIAN);
	var cos=Math.cos(rotationX * this.RADIAN);
	var mat=new Matrix3D();
	mat.set(1, 0, 0, 0, 0, cos, -sin, 0, 0, sin, cos,0,0,0,0,1);
	return this.productMatrix(mat);
};

Matrix3D.prototype.productRotationYMatrix = function(rotationY)
{
	var sin=Math.sin(rotationY * this.RADIAN);
	var cos=Math.cos(rotationY * this.RADIAN);
	var mat=new Matrix3D();
	mat.set(cos, 0, sin, 0, 0, 1, 0, 0, -sin, 0, cos,0,0,0,0,1);
	return this.productMatrix(mat);
};

Matrix3D.prototype.productRotationZMatrix = function(rotationZ)
{
	var sin=Math.sin(rotationZ * RADIAN);
	var cos=Math.cos(rotationZ * RADIAN);
	var mat=new Matrix3D();
	mat.set(cos, -sin, 0, 0, sin, cos,0,0,0,0,1,0,0,0,0,1);
	return this.productMatrix(mat);
};

Matrix3D.prototype.productParallelTransformMatrix = function(x, y, z)
{
	var mat=new Matrix3D();
	mat.set(1, 0, 0, x, 0, 1, 0, y, 0, 0, 1, z,0,0,0,1);
	return this.productMatrix(mat);
};


/*
	Vector3D
*/
var Vector3D = function(x, y, z)
{
	this.x=x;
	this.y=y;
	this.z=z;
	this.w=1;
};

Vector3D.prototype.distance = function(v)
{
	return Math.sqrt((v.x - this.x) * (v.x - this.x) + (v.y - this.y) * (v.y - this.y) + (v.z - this.z) * (v.z - this.z));
};

Vector3D.prototype.subtract = function(v)
{
	var newVec = new Vector3D(0,0,0);
	newVec.x=this.x - v.x;
	newVec.y=this.y - v.y;
	newVec.z=this.z - v.z;
	return newVec;
};

Vector3D.prototype.normalize = function()
{
	var len=Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
	this.x/=len;
	this.y/=len;
	this.z/=len;
};

Vector3D.prototype.innerProduct = function(v)
{
	return (this.x * v.x + this.y * v.y + this.z * v.z);
};

Vector3D.prototype.crossProduct = function(v)
{
	var newVec = new Vector3D(0,0,0);
	newVec.x=this.y * v.z - this.z * v.y;
	newVec.y=this.z * v.x - this.x * v.z;
	newVec.z=this.x * v.y - this.y * v.x;
	return newVec;
};